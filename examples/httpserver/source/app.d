import greatman.server.core;

import std.stdio;
import std.conv;


void handleHttp(HttpRequest req, HttpResponse res)
{
  auto hello = "Hello, World!";

  // set Content-Length
  res.headers.set(ContentLength, hello.length.to!string);

  res.start();
  res.pushBody(hello);
  res.end();

  res.send();
  res.close();
}


void main()
{
  auto server = new Server(4000);
  server.listenThreads(4, &handleHttp);
}
