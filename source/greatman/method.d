module greatman.method;


struct HTTPMethod
{
  string name;
  bool safe;
  bool idempotent;
}


immutable HTTPMethod OPTIONS = { name: "OPTIONS", safe: true, idempotent: true };
immutable HTTPMethod GET = { name: "GET", safe: true, idempotent: true };
immutable HTTPMethod POST = { name: "POST", safe: false, idempotent: false };
immutable HTTPMethod PUT = { name: "PUT", safe: false, idempotent: true };
immutable HTTPMethod DELETE = { name: "DELETE", safe: false, idempotent: true };
immutable HTTPMethod HEAD = { name: "HEAD", safe: true, idempotent: true };
immutable HTTPMethod TRACE = { name: "TRACE", safe: true, idempotent: true };
immutable HTTPMethod CONNECT = { name: "CONNECT", safe: false, idempotent: false };
immutable HTTPMethod PATCH = { name: "PATCH", safe: false, idempotent: false };


// check safe
unittest
{
  assert(GET.safe);
  assert(!POST.safe);
}


// check idempotent
unittest
{
  assert(GET.idempotent);
  assert(PUT.idempotent);
  assert(!POST.idempotent);
}


// check hashable
unittest
{
  HTTPMethod[string] ht;
  ht["get"] = GET;
  assert(ht["get"] == GET);
}
