module greatman.server.core;

import std.socket;
import std.concurrency;
import std.stdio;

import greatman.httpversion;

import greatman.header.core;
import greatman.header.constant;

public import greatman.server.request;
public import greatman.server.response;

import httparse;


// Type: Hanlder
alias Handler = void function(HttpRequest, HttpResponse);



/**
 * TCPListener: Low-level manager for acceptor.
 *
 * NOTE:
 *  Using `cast(shared)` to acceptor for shared by each threads to handle connections.
 *  However, `shared Socket` type is not prepared member-function in Phobos's std.socket
 *  module like listen, accept or bind.
 *  So, it is required to use `cast()` to take off `shared` type.
 */
class TcpListener
{
private:
  shared TcpSocket listener;

public:

  this(ushort port)
  {
    TcpSocket tmpListener = new TcpSocket();
    assert(tmpListener.isAlive);
    tmpListener.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, 1);
    tmpListener.bind(new InternetAddress(port));
    writefln("Listening on port %d.", port);
    listener = cast(shared) tmpListener;
  }

  void listen(int backlog)
  {
    (cast()listener).listen(backlog);
  }

  Socket accept() shared
  {
    Socket sock = (cast()listener).accept();
    scope (failure) {
      if (sock !is null) {
        sock.close;
      }
    }
    return sock;
  }
}


/**
 * Acceptor pool for each threads/connections.
 */
class ListenerPool
{
  TcpListener acceptor;

  this(TcpListener _acceptor)
  {
    acceptor = _acceptor;
  }

  // Runs the acceptor in each threads.
  // Blocks until acceptors are closed.
  void accept(void function(Socket, Handler) worker,
              uint threads, Handler handler)
  in {
    assert(threads != 0);  // threads must > 0.
  }
  body {
    // Begin work in each threads.
    foreach (_; 0..threads) {
      spawnWith(worker, cast(shared)acceptor, handler);
    }

    // TODO: Monitoring whether theareds are living or dead.
  }

  void spawnWith(void function(Socket, Handler) worker,
                 shared(TcpListener) acceptor, Handler handler)
  {
    Tid tid = spawn(&spawnedFunc, thisTid);
    tid.send(worker, acceptor, handler);
  }
}


void spawnedFunc(Tid ownerTid)
{
  bool running = true;

  while (running) {
    auto msg = receiveOnly!(void function(Socket, Handler),
                            shared(TcpListener), Handler);
    auto worker = msg[0];
    auto acceptor = msg[1];
    auto handler = msg[2];
    {
      for (;;) {
        Socket sock = acceptor.accept;
        worker(sock, handler);
      }
    }
  }
}


/**
 * Server: Runs TCP Listener with handler for each connections.
 */
class Server
{
  uint backlog;
  TcpListener listener;

  this(ushort port)
  {
    backlog = 1024;
    listener = new TcpListener(port);
  }

  auto listenThreads(uint threads, Handler handler)
  in {
    assert(threads != 0);
  }
  body {
    listener.listen(backlog);
    withListener(threads, handler);
  }

  void withListener(uint threads, Handler handler)
  {
    auto pool = new ListenerPool(listener);
    pool.accept(&handleConnection, threads, handler);
    for(;;) { /* I'm sleeping. */ }
  }
}


void handleConnection(Socket stream, Handler handler)
{
  scope(exit) stream.close;

  bool keepalive = true;

  while (keepalive) {
    if (!stream.isAlive) {
      break;
    }
    auto req = new HttpRequest(stream);
    auto res = new HttpResponse(stream);

    keepalive = shouldKeepalive(req.http_version,
                                req.headers);

    res.http_version = req.http_version;
    handler(req, res);
  }
}


bool shouldKeepalive(ubyte[] http_version, HttpHeaders headers)
{
  foreach(key, value; headers.data) {
    if (key == Connection) {
      if (value == cast(ubyte[]) "Close"
          && http_version == HTTP_VERSION_11) {
        return false;
      }
      if (value != cast(ubyte[]) "Keep-Alive"
          && http_version == HTTP_VERSION_10) {
        return false;
      }
    }
  }
  return true;
}
