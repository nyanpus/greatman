module greatman.server.response;

import std.socket;
import std.datetime;
import std.conv;
import std.array;
import std.stdio;

import greatman.status;
import greatman.httpversion;
import greatman.util;

import greatman.header.core;
import greatman.header.httpdate;
public import greatman.header.constant;

import httparse;


class HttpResponse
{
  StatusCode statusCode;
  ubyte[] http_version;
  HttpHeaders headers;
  Socket stream;
  Appender!(ubyte[]) buf;

  this(Socket _stream)
  {
    statusCode = OK;
    http_version = HTTP_VERSION_11;
    headers = new HttpHeaders;
    stream = _stream;

    buf = appender!(ubyte[])();
    buf.reserve(1024);
  }

  void start()
  {
    buf.put(http_version);
    buf.put(cast(ubyte[]) statusCode);
    buf.put(CRLF);

    if (!(headers.has(DateHeader))) {
      headers.set(DateHeader, new HttpDate(Clock.currTime));
    }

    bool chunked = false;

    if (headers.has(ContentLength)) {
      chunked = false;
    }

    if (chunked) {
      if (headers.has(TransferEncoding)) {
        headers.set(ContentLength, headers.get(ContentLength) ~ ", chunked");
      }
      else {
        headers.set(ContentLength, "chunked");
      }
    }
    buf.put(cast(ubyte[]) headers.toString);
    buf.put(CRLF);
  }

  void pushBody(ubyte[] resBody)
  {
    buf.put(resBody);
  }

  void pushBody(string resBody)
  {
    pushBody(cast(ubyte[]) resBody);
  }

  void end()
  {
    buf.put(CRLF);
    buf.put(CRLF);
  }

  ptrdiff_t send()
  {
    return stream.send(cast(const void[]) buf.data);
  }

  void close()
  {
    stream.close();
  }
}
