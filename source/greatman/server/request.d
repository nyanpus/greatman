module greatman.server.request;

import std.socket;
import httparse;

import greatman.httpreader;
import greatman.header.core;
import greatman.header.constant;


auto httpParseRequest(ubyte[] buf)
{
  Header*[] arr = [new Header(null, null),
                   new Header(null, null),
                   new Header(null, null),
                   new Header(null, null)];

  auto headers = new Headers(arr);
  auto req = new httparse.Request(headers);
  req.parse(buf);
  return req;
}



class HttpRequest
{
  Request request;
  string method;
  ubyte[] http_version;
  HttpHeaders headers;
  HttpReader* reader;
  ubyte[1024] buf;

  this(Socket stream)
  {
    stream.receive(buf[]);
    request = httpParseRequest(buf);

    headers = HttpHeaders.fromRaw(request.headers);
    method = request.method;
    http_version = cast(ubyte[]) request.http_version;

    if (method == "GET" || method == "HEAD") {
      reader = new HttpReader(stream, RequestType.Emtpy);
    }
    else if (headers.has(ContentLength)) {
      reader = new HttpReader(stream, RequestType.Sized);
    }
    else if (headers.has(TransferEncoding)) {
      reader = new HttpReader(stream, RequestType.Chunked);
    }
    else {
      reader = new HttpReader(stream, RequestType.Emtpy);
    }
  }
}
