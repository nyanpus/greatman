module greatman.httpreader;

import std.socket : Socket;


enum RequestType
{
  Emtpy, Chunked, Sized,
}


struct HttpReader
{
  Socket stream;
  RequestType type;
}
