module greatman.status;

import std.conv : to;


struct StatusCode
{
  ubyte code;
  string reason;

  T opCast(T)() if (is(T == string))
  {
    return toString;
  }

  T opCast(T)() if (is(T == ubyte[]))
  {
    return cast(ubyte[]) toString;
  }

  T to(T)() if (is(T == string))
  {
    return toString;
  }

  string toString()
  {
    return code.to!string ~ " " ~ reason;
  }

  alias toString this;
}

static StatusCode OK = { code: 200, reason: "OK" };


unittest
{
  assert("200 OK" == OK);
  assert("200 OK" == cast(string)OK);
  assert("200 OK" == OK.to!string);
}
