module greatman.buffer;

import std.algorithm;

debug(buffer) import std.stdio;


immutable INIT_BUFFER_SIZE = 4096;
immutable MAX_BUFFER_SIZE = 8192 + 4096 * 100;  // 400K + 2K


interface Reader
{
  size_t read(ubyte[]);
}


class BufferedReader
{
  Reader inner;
  ubyte[] data;
  size_t offset;
  size_t cap;

  this(Reader reader)
  {
    inner = reader;
    data.length = INIT_BUFFER_SIZE;
    offset = 0;
    cap = 0;
  }

  Reader getReader()
  {
    return inner;
  }

  ubyte[] getBuf()
  {
    if (offset < cap) {
      return data[offset .. cap];
    }
    return [];
  }


  ulong readIntoBuf()
  {
    ulong nread = 0;

    reserve();
    if (cap < data.length) {
      nread = inner.read(data[cap .. $]);
      cap += nread;
    }
    return nread;
  }

  void reserve()
  {
    if (cap == data.length) {
      data.length = min(cap * 4, MAX_BUFFER_SIZE);  // extend buffer length.
    }
  }

  void consume(uint amt)
  {
    offset = min(offset + amt, cap);
    if (offset == cap) {
      offset = 0;
      cap = 0;
    }
  }
}



unittest
{
  import core.stdc.string : memcpy;

  class SlowRead : Reader
  {
    ulong state;

    this() {
      state = 0;
    }

    size_t read(ubyte[] data)
    {
      ubyte[] arr;
      switch(state % 3) {
      case 0: {
        arr = cast(ubyte[]) "foo";
        break;
      }
      case 1: {
        arr = cast(ubyte[]) "bar";
        break;
      }
      default: {
        arr = cast(ubyte[]) "baz";
        break;
      }
      }
      ++state;
      memcpy(cast(void**)data, cast(void**)arr, (ubyte[]).sizeof);
      return arr.length;
    }
  }

  scope br = new BufferedReader(new SlowRead);
  auto nread = br.readIntoBuf();
  assert(nread == 3);
  br.consume(1);
  debug(buffer) writeln(cast(string) br.get_buf);
  assert(br.getBuf == cast(ubyte[])"oo");
  nread = br.readIntoBuf;
  assert(nread == 3);
  nread = br.readIntoBuf;
  assert(nread == 3);
  assert(br.getBuf == cast(ubyte[]) "oobarbaz");
  br.consume(5);
  assert(br.getBuf == cast(ubyte[]) "baz");
  br.consume(3);
  assert(br.getBuf == cast(ubyte[]) "");
  assert(br.offset == 0);
  assert(br.cap == 0);
}
