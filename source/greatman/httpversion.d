module greatman.httpversion;

import std.conv : to;

static ubyte[] HTTP_VERSION_10 = ['H', 'T', 'T', 'P', '/', '1', '.', '0'];
static ubyte[] HTTP_VERSION_11 = ['H', 'T', 'T', 'P', '/', '1', '.', '1'];
