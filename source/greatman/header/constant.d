module greatman.header.constant;

static string DateHeader = "Date";
static string ContentLength = "Content-Length";
static string TransferEncoding = "Transfer-Encoding";
static string Connection = "Connection";
