module greatman.header.core;

import greatman.util;
import httparse : Headers;


class HttpHeaders
{
  string[string] data;

  void set(string key, string value)
  {
    data[key] = value;
  }

  bool has(string key)
  {
    string* p;
    p = (key in data);
    return (p !is null);
  }

  string get(string key)
  {
    return data[key];
  }

  override string toString()
  {
    string buf;
    foreach (headerName, headerValue; data) {
      buf ~= headerName ~ ": " ~ headerValue ~ "\r\n";
    }
    return buf;
  }

  static typeof(this) fromRaw(Headers raw)
  {
    auto headers = new HttpHeaders;
    foreach (header; raw) {
      string name = header.name;
      string value = cast(string) header.value;
      headers.set(name, value);
    }
    return headers;
  }
}