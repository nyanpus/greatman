module greatman.header.util;

import std.traits;
import std.utf;


struct FixedAppender(ArrayType : E[], size_t NELEM, E)
{
  alias ElemType = Unqual!E;
  private
  {
    ElemType[NELEM] m_data;
    size_t m_fill;
  }

  void clear()
  {
    m_fill = 0;
  }

  void put(E el)
  {
    m_data[m_fill++] = el;
  }

  static if( is(ElemType == char) ){
    void put(dchar el)
    {
      if( el < 128 ) put(cast(char)el);
      else {
        char[4] buf;
        auto len = encode(buf, el);
        put(cast(ArrayType)buf[0 .. len]);
      }
    }
  }

  void put(ArrayType arr)
  {
    m_data[m_fill .. m_fill+arr.length] = (cast(ElemType[])arr)[];
    m_fill += arr.length;
  }

  @property ArrayType data()
  {
    return cast(ArrayType)m_data[0 .. m_fill];
  }

  static if (!is(E == immutable)) {
    void reset()
    {
      m_fill = 0;
    }
  }
}
