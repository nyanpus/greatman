module greatman.header.httpdate;

import std.datetime;
import std.array;
import core.time;

import greatman.header.util;


private void writeDecimal2(R)(ref R dst, uint n)
{
  auto d1 = n % 10;
  auto d2 = (n / 10) % 10;
  dst.put(cast(char)(d2 + '0'));
  dst.put(cast(char)(d1 + '0'));
}


private void writeDecimal(R)(ref R dst, uint n)
{
  if( n == 0 ){
    dst.put('0');
    return;
  }

  // determine all digits
  uint[10] digits;
  int i = 0;
  while( n > 0 ){
    digits[i++] = n % 10;
    n /= 10;
  }

  // write out the digits in reverse order
  while( i > 0 ) dst.put(cast(char)(digits[--i] + '0'));
}


private immutable monthStrings = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


/**
   Writes an RFC-822/5322 date string to the given output range.
*/
void writeRFC822DateString(R)(ref R dst, SysTime time)
{
  static immutable dayStrings = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  dst.put(dayStrings[time.dayOfWeek]);
  dst.put(", ");
  writeDecimal2(dst, time.day);
  dst.put(' ');
  dst.put(monthStrings[time.month-1]);
  dst.put(' ');
  writeDecimal(dst, time.year);
}


/**
   Writes an RFC-822 time string to the given output range.
*/
void writeRFC822TimeString(R)(ref R dst, SysTime time)
{
  writeDecimal2(dst, time.hour);
  dst.put(':');
  writeDecimal2(dst, time.minute);
  dst.put(':');
  writeDecimal2(dst, time.second);

  if( time.timezone == UTC() ) dst.put(" GMT");
  else {
    auto now = Clock.currStdTime();
    auto offset = cast(int)((time.timezone.utcToTZ(now) - now) / 600_000_000);
    dst.put(' ');
    dst.put(offset >= 0 ? '+' : '-');
    if( offset < 0 ) offset = -offset;
    writeDecimal2(dst, offset / 60);
    writeDecimal2(dst, offset % 60);
  }
}


/**
   Writes an RFC-822 date+time string to the given output range.
*/
void writeRFC822DateTimeString(R)(ref R dst, SysTime time)
{
  writeRFC822DateString(dst, time);
  dst.put(' ');
  writeRFC822TimeString(dst, time);
}


/**
   Returns the RFC-822 time string representation of the given time.
*/
string toRFC822TimeString(SysTime time)
{
  auto ret = new FixedAppender!(string, 14);
  writeRFC822TimeString(ret, time);
  return ret.data;
}


/**
   Returns the RFC-822/5322 date string representation of the given time.
*/
string toRFC822DateString(SysTime time)
{
  auto ret = new FixedAppender!(string, 16);
  writeRFC822DateString(ret, time);
  return ret.data;
}


/**
   Returns the RFC-822 date+time string representation of the given time.
*/
string toRFC822DateTimeString(SysTime time)
{
  auto ret = new FixedAppender!(string, 31);
  writeRFC822DateTimeString(ret, time);
  return ret.data;
}



class HttpDate
{
  SysTime time;

  this(SysTime _time)
  {
    time = _time;
  }

  T opCast(T)() if (is(T == string))
  {
    return toString;
  }

  T to(T)() if (is(T == string))
  {
    return toString;
  }

  override string toString()
  {
    return toRFC822DateTimeString(time);
  }

  alias toString this;
}


unittest
{
  import std.exception;

  auto tz = new immutable SimpleTimeZone(hours(9));
  SysTime time = SysTime(DateTime(1990, 1, 6, 12, 14, 19), tz);
  HttpDate date = new HttpDate(time);
  assertNotThrown!DateTimeException(parseRFC822DateTime(date));
  assert("Sat, 06 Jan 1990 12:14:19 +0900" == date.to!string);
}
