module greatman.exception;


// About HTTP exception.


// Exception for invalid HTTP method.
class HttpMethodError : Exception
{
  this(string msg) {
    super(msg);
  }
}


// Exception for invalid URL.
class UrLParseError : Exception
{
  this(string msg) {
    super(msg);
  }
}


// Exception for invalid HTTP version.
class HTTPVersionError : Exception
{
  this(string msg) {
    super(msg);
  }
}


// Exception for invalid HTTP header.
class HTTPHeaderError : Exception
{
  this(string msg) {
    super(msg);
  }
}


// Exception for too large header length.
class HTTPTooLargeError : Exception
{
  this(string msg) {
    super(msg);
  }
}


// Exception for invalid status
class HTTPStatusError : Exception
{
  this(string msg) {
    super(msg);
  }
}


// Exception for IO
class HTTPIoException : Exception
{
  this(string msg) {
    super(msg);
  }
}
