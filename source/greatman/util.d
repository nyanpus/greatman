module greatman.util;


static immutable char CR = '\r';
static immutable char LF = '\n';
static ubyte[] CRLF = ['\r', '\n'];