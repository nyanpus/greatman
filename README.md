# Greatman [![Build Status](https://secure.travis-ci.org/kubo39/greatman.png?branch=develop)](http://travis-ci.org/kubo39/greatman)

Greatman is HTTP Library for D.

## Overview

Greatman is modern HTTP implementation written in and for D.
